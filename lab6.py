#Подключение библиотек
import math

while True:
	while True:
		#Проверка вводимых данных
		try:
			#Решение пользователя: начать подсчет функции или нет
			answer = int(input('Начать подсчет функции?\n1 - "ДА"\n0 - "НЕТ"\nВаш ответ: '))
			print()
			if answer == 1:
				break
			elif math.isclose(answer, 0, abs_tol = 0.01):
				exit()
			else:
				print('Ответьте "0" или "1"!\n')
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	while True:
		#Проверка вводимых данных
		try:
			#Ввод границ изменения "x"
			print('Введите границы изменения "x":')
			x1 = int(input('x1 = '))
			x2 = int(input('x2 = '))
			print()
			if x1 > x2:
				print('Значение "x1" должно быть меньше значения "x2"!\n')
			else:
				break
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	while True:
		#Проверка вводимых данных
		try:
			#Блок ввода данных программы
			a = float(input('Введите a: '))
			while True:
				try:
					x = float(input('Введите x: '))
					print()
					if x < x1 or x > x2:
						print('Введенное значение "x" не принадлежит заданным границам!\n')
					else:
						add_x = x
						break
				except ValueError:
					print('\nВведен неверный формат данных!\n')
			break
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	while True:
		#Проверка вводимых данных
		try:
			#Ввод кол-ва шагов изменения "x"
			count_step = int(input('Введите кол-во шагов изменения "x": '))
			if count_step > 0:
				print()
				break
			else:
				print('\nКол-во шагов не может быть меньше нуля или равно ему!\n')
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	#Подсчет длины шага
	step = (x2 - x) / count_step

	#Ввод переменной для работы цикла
	i = 0

	#Ввод данных массива
	mas_x_func = []

	#Блок подсчета функции G
	if math.isclose(step, 0, abs_tol = 0.01):
		g1 = (-20) * a ** 2 + 28 * a * x + 3 * x ** 2
		if not math.isclose(g1, 0, abs_tol = 0.01):
			g2 = 4 * ((-4) * a ** 2 - a * x + 5 * x ** 2)
			G = g2 / g1
			mas_x_func.append((x, G))
		else:
			mas_x_func.append((x, None))
	else:	
		while i != count_step:
			g1 = (-20) * a ** 2 + 28 * a * x + 3 * x ** 2
			if not math.isclose(g1, 0, abs_tol = 0.01):
				g2 = 4 * ((-4) * a ** 2 - a * x + 5 * x ** 2)
				G = g2 / g1
				mas_x_func.append((x, G))
			else:
				mas_x_func.append((x, None))
			x += step
			i += 1

	#Вывод функции G
	print('Функция G')
	for elem in mas_x_func:
		if elem[1] == None:
			print('x = {:.5f} Ошибка! Входные значения не принадлежат области определения функции.'.format(elem[0]))
		else:
			print('x = {0:.5f} G = {1:.5f}'.format(elem[0], elem[1]))
	
	mas_x_func = [] #Обнуление массива
	i = 0 #Обнуление переменной для работы цикла
	x = add_x #Возвращение начального значения "x"
	
	#Блок подсчета функции F
	if math.isclose(step, 0, abs_tol = 0.01):
		F = math.atan(24 * a ** 2 - 25 * a * x + 6 * x ** 2)
		mas_x_func.append((x, F))
	else:		
		while i != count_step:
			F = math.atan(24 * a ** 2 - 25 * a * x + 6 * x ** 2)
			mas_x_func.append((x, F))
			x += step
			i += 1

	#Вывод функции F
	print('\nФункция F')
	for elem in mas_x_func:
		if elem[1] == None:
			print('x = {:.5f} Ошибка! Входные значения не принадлежат области определения функции.'.format(elem[0]))
		else:
			print('x = {0:.5f} F = {1:.5f}'.format(elem[0], elem[1]))

	mas_x_func = [] #Обнуление массива
	i = 0 #Обнуление переменной для работы цикла
	x = add_x #Возвращение начального значения "x"

	#Блок подсчета функции Y
	if math.isclose(step, 0, abs_tol = 0.01):
		y1 = 2 * a ** 2 - 7 * a * x + 6 * x ** 2 + 1
		if not y1 < 0:
			Y = math.log(y1)
			mas_x_func.append((x, Y))
		else:
			mas_x_func.append((x, None))
	else:
		while i != count_step:
			y1 = 2 * a ** 2 - 7 * a * x + 6 * x ** 2 + 1
			if not y1 < 0:
				Y = math.log(y1)
				mas_x_func.append((x, Y))
			else:
				mas_x_func.append((x, None))
			x += step
			i += 1

	#Вывод функции Y
	print('\nФункция Y')
	for elem in mas_x_func:
		if elem[1] == None:
			print('x = {:.5f} Ошибка! Входные значения не принадлежат области определения функции.'.format(elem[0]))
		else:
			print('x = {0:.5f} Y = {1:.5f}'.format(elem[0], elem[1]))

	print()
